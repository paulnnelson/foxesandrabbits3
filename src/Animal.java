/**
 * Animal is an abstract superclass for animals. It provides features common to
 * all animals, that aren't represented in the Actor superclass. Specifically,
 * age and whether they are infected.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 */
public abstract class Animal extends Actor {
	// The animal's age.
	protected int age;
	// Whether the animal is infected by the virus, or not.
	protected boolean infected;

	/**
	 * Create a new animal with age zero (a new born).
	 */
	public Animal() {
		super();
		age = 0;
		infected = false;
	}

	/**
	 * Return the animal's age.
	 * @return The animal's age.
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Set the animal's age.
	 * @param age The animal's age.
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * Return whether the animal is infected or not.
	 * @return The animal's infected status.
	 */
	public boolean getInfected() {
		return infected;
	}

	/**
	 * Set the animal as infected.
	 */
	public void setInfected() {
		infected = true;
	}

	/**
	 * Abstract class similar to act, but defines the animal's actions in
	 * regards to being infected.
	 * @param currentField: current field of simulation
	 * @param updatedField: future field of simulation
	 */
	abstract public void actVirus(Field currentField, Field updatedField);
}
