import java.util.Iterator;
import java.util.Random;

/**
 * Subclass of actor that has its own actions.
 * The virus randomly appears on the original field.
 * It can give birth to new viruses and spread to rabbits.
 *
 */
public class Virus extends Actor {

	private static final double BREEDING_PROBABILITY = 0.2;
	private static final Random rand = new Random();

	/**
	 * Constructor of virus.
	 * Has no unique fields outside of it's Actor superclass.
	 */
	public Virus() {
		super();
	}

	/**
	 * The actions that the Virus generally takes.
	 * The virus can breed and also call the infect method to infect rabbits.
	 */
	public void act(Field currentField, Field updatedField) {

		if (isAlive()) {
			//attempt to give birth to new viruses in 1 adjacent location
			int births = breed();
			for (int b = 0; b < births; b++) {
				Location newOne = updatedField.freeAdjacentLocation(getLocation());
				if (newOne != null) {
					Virus newVirus = new Virus();
					newVirus.setLocation(newOne);
					updatedField.place(newVirus);
				}
			}
			// call the infect method that can spread the virus to rabbits.
			infect(currentField, getLocation());
		}

	}

	/**
	 * The best way for the virus to spread.
	 * If there are any rabbits near the virus they have a 50% chance of being infected.
	 * @param field
	 * @param location
	 */
	public void infect(Field field, Location location)
	{
    Iterator<Location> adjacentLocations = field.adjacentLocations(location);
    while(adjacentLocations.hasNext()) {
    	Location where = adjacentLocations.next();
    	Actor actor = field.getActorAt(where);
    	// if the actor is a rabbit, the actor is cast as a rabbit
    	// and has a 50% chance of being infected with the virus.
    	if(actor instanceof Rabbit) {
    		Rabbit rabbit = (Rabbit) actor;
    		if(rabbit.isAlive()) {
    			if(rand.nextInt(100) < 50)
    			{
    				rabbit.setInfected();
    				System.out.println("Rabbit infected!");
    			}else{
    				System.out.println("Rabbit avoids infection!");
    			}
    			
    		}
    	}
    }
	}

	/**
	 * Generate a number representing the number of births, if it can breed.
	 * 
	 * @return The number of births (may be zero).
	 */
	private int breed() {
		int births = 0;
		if (rand.nextDouble() <= BREEDING_PROBABILITY) {
			births = 1;
		}
		return births;
	}
}
