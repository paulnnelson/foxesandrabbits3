import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * A simple predator-prey simulator, based on a field containing rabbits and
 * foxes.
 * 
 * @author Paul Nelson
 */
public class Simulator {
	// Constants representing configuration information for the simulation.
	// The default width for the grid.
	private static final int DEFAULT_WIDTH = 50;
	// The default depth of the grid.
	private static final int DEFAULT_DEPTH = 50;
	// The probability that a fox will be created in any given grid position.
	private static final double FOX_CREATION_PROBABILITY = 0.10;
	// The probability that a rabbit will be created in any given grid position.
	private static final double RABBIT_CREATION_PROBABILITY = 0.08;
	// The probability that a virus will be created in any given grid position.
	private static final double VIRUS_CREATION_PROBABILITY = 0.01;

	// The current state of the field.
	private Field field;
	// A second field, used to build the next stage of the simulation.
	private Field updatedField;
	// The current step of the simulation.
	private int step;
	// A graphical view of the simulation.
	private SimulatorView view;

	// Added this variable for use by the thread.
	private int numberSteps;

	// The main window to display the simulation (and your buttons, etc.).
	JButton runOneButton;
	JButton runOneHundredButton;
	JButton runLongButton;
	JButton insertButton;
	JButton resetButton;
	JTextField jtf;
	private JFrame mainFrame;

	/**
	 * Construct a simulation field with default size.
	 */
	public Simulator() {
		this(DEFAULT_DEPTH, DEFAULT_WIDTH);
	}

	public static void main(String[] args) {
		// Create the simulator
		Simulator s = new Simulator();
	}

	/**
	 * Create a simulation field with the given size.
	 * 
	 * @param depth
	 *          Depth of the field. Must be greater than zero.
	 * @param width
	 *          Width of the field. Must be greater than zero.
	 */
	public Simulator(int depth, int width) {
		if (width <= 0 || depth <= 0) {
			System.out.println("The dimensions must be greater than zero.");
			System.out.println("Using default values.");
			depth = DEFAULT_DEPTH;
			width = DEFAULT_WIDTH;

		}
		field = new Field(depth, width);
		updatedField = new Field(depth, width);

		// Create a view of the state of each location in the field.
		view = new SimulatorView(depth, width);
		view.setColor(Rabbit.class, Color.orange);
		view.setColor(Fox.class, Color.blue);
		view.setColor(Virus.class, Color.black);

		// Create the JFrame to add everything to.
		mainFrame = new JFrame();

		mainFrame.setTitle("Fox and Rabbit Simulation");

		// Add window listener so it closes properly.
		// when the "X" in the upper right corner is clicked.
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// Add a button to run 1 steps.
		runOneButton = new JButton("Run 1 step");
		runOneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulateOneStep();
			}
		});

		// Add a button that runs 100 steps.
		runOneHundredButton = new JButton("Run 100 steps");
		runOneHundredButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulate(100);
			}
		});

		// Add a button that runs a long simulation (500 steps).
		runLongButton = new JButton("Run Long");
		runLongButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runLongSimulation();
			}
		});

		// Add a button that runs a given number of steps.
		// This number is given from the text field under the button.
		// In this code, the text field is "jtf".
		insertButton = new JButton("Run # steps below");
		jtf = new JTextField();
		insertButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// calls the getTextField method which will return
				// the number of steps a user specifically wants.
				int x = getTextField();
				simulate(x);
			}
		});

		// Add a button that resets the simulation.
		resetButton = new JButton("Reset Simulation");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reset();
			}
		});

		// Adding things to a JFrame first requires that you get the
		// content pane. Notice you don't do with with a JPanel.
		Container contents = mainFrame.getContentPane();
		// Adds the simulator view to the bottom of the pane.
		contents.add(view, BorderLayout.SOUTH);
		// Creates a box that will store all of the buttons.
		Box topBox = new Box(0);
		// Creates a box that will store the run specific # of steps
		// and the text field below it.
		Box runXStepsBox = new Box(1);
		runXStepsBox.add(insertButton);
		runXStepsBox.add(jtf);

		// Add the buttons and box to the box.
		topBox.add(runOneButton);
		topBox.add(runOneHundredButton);
		topBox.add(runLongButton);
		topBox.add(runXStepsBox);
		topBox.add(resetButton);

		// Add the box to the top of the container.
		contents.add(topBox, BorderLayout.CENTER);

		// Create a menu bar for the container.
		JMenuBar menuBar = new JMenuBar();
		// Create file menu that will store the "File->Exit".
		// Clicking on file and then exit will exit the application.
		JMenu fileMenu = new JMenu("File");
		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		fileMenu.add(exit);

		// Create help menu that will store the "Help->About".
		// Click on Help and then about will bring up information.
		JMenu helpMenu = new JMenu("Help");
		JMenuItem about = new JMenuItem("About");
		about.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(about,
						"The author of this application is Paul Nelson. This took the original foxes and rabbits simulation and added a virus for more fun.");
			}
		});
		helpMenu.add(about);

		// Add the file menu and help menu to the menu bar.
		// Add the menu bar to the top of the container.
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		contents.add(menuBar, BorderLayout.NORTH);

		// You always need to call pack on a JFrame.
		mainFrame.pack();

		// For very subtle reasons, this MUST go after the pack statement above.
		reset();

		// You always need to call setVisible(true) on a JFrame.
		mainFrame.setVisible(true);
	}

	/**
	 * Run the simulation from its current state for a reasonably long period,
	 * e.g. 500 steps.
	 */
	public void runLongSimulation() {
		simulate(500);
	}

	/**
	 * Run the simulation from its current state for the given number of steps.
	 * Stop before the given number of steps if it ceases to be viable. This has
	 * been modified so it uses a thread--this allows it to work in conjunction
	 * with Swing
	 * 
	 * Added the setting of all buttons to false while the simulation is running.
	 * 
	 * @param numSteps
	 *          How many steps to run for.
	 */
	public void simulate(int numSteps) {
		numberSteps = numSteps;
		// Create a thread
		Thread runThread = new Thread() {
			// When the thread runs, it will simulate numberSteps steps.
			public void run() {
				// Disable the button until the simulation is done.
				runOneButton.setEnabled(false);
				runOneHundredButton.setEnabled(false);
				runLongButton.setEnabled(false);
				insertButton.setEnabled(false);
				resetButton.setEnabled(false);

				for (int step = 1; step <= numberSteps && view.isViable(field); step++) {
					simulateOneStep();
				}
				// Now re-enable the button
				runOneButton.setEnabled(true);
				runOneHundredButton.setEnabled(true);
				runLongButton.setEnabled(true);
				insertButton.setEnabled(true);
				resetButton.setEnabled(true);
			}
		};
		// Start the thread
		runThread.start();
		// Now this method exits, allowing the GUI to update.
	}

	/**
	 * Run the simulation from its current state for a single step. Iterate over
	 * the whole field updating the state of each fox and rabbit.
	 * 
	 * Added functionality that repaints after every simulation step.
	 */
	public void simulateOneStep() {
		step++;

		// let all animals act
		ArrayList<Actor> actors = field.getActors();
		Collections.shuffle(actors); // to randomize the order they act.
		for (Iterator<Actor> it = actors.iterator(); it.hasNext();) {
			Actor actor = it.next();
			if (actor instanceof Animal) {
				if (step > 1) {
					((Animal) actor).actVirus(field, updatedField);
				}
			}
			actor.act(field, updatedField);
		}

		// Swap the field and updatedField at the end of the step.
		Field temp = field;
		field = updatedField;
		updatedField = temp;
		updatedField.clear();

		// Display the new field on screen.
		view.showStatus(step, field);

		int i = 0;
		for (Iterator<Actor> it = actors.iterator(); it.hasNext();) {
			Actor actor = it.next();
			if (actor instanceof Animal) {
				Animal animal = (Animal) actor;
				if (animal.isAlive()) {
					if (animal.getInfected() == true) {
						i++;
					}
				}
			}
		}
		System.out.println("Number of infected is: " + i);
		view.repaint();
	}

	/**
	 * Reset the simulation to a starting position.
	 */
	public void reset() {
		step = 0;
		field.clear();
		updatedField.clear();
		populate(field);

		// Show the starting state in the view.
		view.showStatus(step, field);
	}

	/**
	 * Populate a field with foxes and rabbits.
	 * 
	 * @param field
	 *          The field to be populated.
	 */
	private void populate(Field field) {
		Random rand = new Random();
		field.clear();
		for (int row = 0; row < field.getDepth(); row++) {
			for (int col = 0; col < field.getWidth(); col++) {
				if (rand.nextDouble() <= FOX_CREATION_PROBABILITY) {
					Fox fox = new Fox(true);
					fox.setLocation(row, col);
					field.place(fox);
				}
				else if (rand.nextDouble() <= RABBIT_CREATION_PROBABILITY) {
					Rabbit rabbit = new Rabbit(true);
					rabbit.setLocation(row, col);
					field.place(rabbit);
				}
				else if (rand.nextDouble() <= VIRUS_CREATION_PROBABILITY) {
					Virus virus = new Virus();
					virus.setLocation(row, col);
					field.place(virus);
				}
				// else leave the location empty.
			}
		}
	}

	public int giveNumberOfSteps() {
		return numberSteps;
	}

	
	/**
	 * Return the number of steps entered in the text field.
	 * This is to be passed to a button when a user enters it.
	 * 
	 * @return steps : the number of steps entered in the text field.
	 * 	This int is to be used in the simulation when a user wants to
	 * 	simulate a certain number of steps.
	 */
	public int getTextField() {
		// The value originally stored in the text field is a string
		// of numerics -- Ex: "100"
		// Try to turn this into an integer. Otherwise, return 0
		// to ensure nothing is done.
		try {
			int steps = Integer.parseInt(jtf.getText());
			return steps;
		}
		catch (Exception e) {
			return 0;
		}
	}
}
