/**
 * Actor is an abstract superclass for Animals and Virus. It provides features
 * common to each of these, specifically whether they are alive and the location
 */
public abstract class Actor {

	// Whether the Actor is alive/active or not
	protected boolean alive;
	// The actor's position
	protected Location location;

	public Actor() {
		alive = true;
	}

	/**
	 * Make this Actor act - do whatever it wants/needs to do.
	 * @param currentField The field currently occupied.
	 * @param updatedField The field to transfer to.
	 */
	abstract public void act(Field currentField, Field updatedField);

	/**
	 * Check whether the actor is alive or not.
	 * @return True if the actor is still alive.
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * Tell the actor that it's dead now :(
	 */
	public void setDead() {
		alive = false;
	}

	/**
	 * Return the actor's location.
	 * @return The actor's location.
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * Set the actor's location.
	 * @param row The vertical coordinate of the location.
	 * @param col The horizontal coordinate of the location.
	 */
	public void setLocation(int row, int col) {
		this.location = new Location(row, col);
	}

	/**
	 * Set the actor's location.
	 * @param location The actor's location.
	 */
	public void setLocation(Location location) {
		this.location = location;
	}
}
