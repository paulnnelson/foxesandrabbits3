import java.util.Iterator;
import java.util.Random;

/**
 * A simple model of a rabbit. Rabbits age, move, breed, and die.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 */
public class Rabbit extends Animal {
	// Characteristics shared by all rabbits (static fields).

	// The age at which a rabbit can start to breed.
	private static final int BREEDING_AGE = 5;
	// The age to which a rabbit can live.
	private static final int MAX_AGE = 50;
	// The likelihood of a rabbit breeding.
	private static final double BREEDING_PROBABILITY = 0.15;
	// The maximum number of births.
	private static final int MAX_LITTER_SIZE = 5;
	// A shared random number generator to control breeding.
	private static final Random rand = new Random();

	// Individual characteristics (instance fields).

	/**
	 * Create a new rabbit. A rabbit may be created with age zero (a new born)
	 * or with a random age.
	 * 
	 * @param randomAge
	 *            If true, the rabbit will have a random age.
	 */
	public Rabbit(boolean randomAge) {
		super();
		if (randomAge) {
			setAge(rand.nextInt(MAX_AGE));
		}
	}

	/**
	 * This is what the rabbit does most of the time - it runs around. Sometimes
	 * it will breed or die of old age.
	 * 
	 * @param currentField
	 *            The field currently occupied.
	 * @param updatedField
	 *            The field to transfer to.
	 */
	public void act(Field currentField, Field updatedField) {
		incrementAge();
		if (isAlive()) {
			int births = breed();
			// Attempt to have births children
			for (int b = 0; b < births; b++) {
				// New code (Added January 28, 2013 by CAC)
				Location newOne = updatedField
						.freeAdjacentLocation(getLocation());
				if (newOne != null) {
					// Only create the new rabbit if there is somewhere to put
					// it.
					Rabbit newRabbit = new Rabbit(false);
					newRabbit.setLocation(newOne);
					updatedField.place(newRabbit);
				}
			}
			Location newLocation = updatedField
					.freeAdjacentLocation(getLocation());
			// Only transfer to the updated field if there was a free location
			if (newLocation != null) {
				setLocation(newLocation);
				updatedField.place(this);
			} else {
				// can neither move nor stay - overcrowding - all locations
				// taken
				setDead();
			}
		}
	}

	/**
	 * The actions the Rabbit takes after being invected. The rabbit will both
	 * fight the virus and spread the virus to other rabbits.
	 */
	public void actVirus(Field currentField, Field updatedField) {
		fightVirus();
		spreadVirus(currentField, getLocation());
	}

	/**
	 * Increase the age. This could result in the rabbit's death.
	 */
	private void incrementAge() {
		setAge(getAge() + 1);
		if (getAge() > MAX_AGE) {
			setDead();
		}
	}

	/**
	 * Generate a number representing the number of births, if it can breed.
	 * 
	 * @return The number of births (may be zero).
	 */
	private int breed() {
		int births = 0;
		if (canBreed() && rand.nextDouble() <= BREEDING_PROBABILITY) {
			births = rand.nextInt(MAX_LITTER_SIZE) + 1;
		}
		return births;
	}

	/**
	 * @return A string representation of the rabbit.
	 */
	public String toString() {
		return "Rabbit, age " + getAge();
	}

	/**
	 * A rabbit can breed if it has reached the breeding age.
	 */
	private boolean canBreed() {
		return getAge() >= BREEDING_AGE;
	}

	/**
	 * How the rabbit fights the virus. If the rabbit is past middle age, it
	 * automatically dies. If not, the rabbit has a chance of dying and a chance
	 * of being healed.
	 */
	public void fightVirus() {
		if (infected == true) {
			//if the rabbit is holder than half the max age, they immediately die.
			if (age > MAX_AGE/2) {
				setDead();
				System.out.println("A rabbit died of a virus and old age!");
			// if it is a young rabbit, they have a 20% chance of dying
			// and a 50% chance of being cured of the virus.
			} else {
				int roulette = rand.nextInt(100);
				if (roulette < 20) {
					setDead();
					System.out.println("A rabbit died from the virus!");
				} else if (roulette < 70) {
					System.out.println("A rabbit was cured of the virus!");
					infected = false;
				}
			}
		}
	}

	/**
	 * How the rabbit can spread the virus, only to other rabbits.
	 * All rabbits in adjacent locations have a 50% chance of receiving the virus.
	 * @param field
	 * @param location
	 */
	public void spreadVirus(Field field, Location location) {
		//Iterate over all adjacent locations
		Iterator<Location> adjacentLocations = field
				.adjacentLocations(location);
		while (adjacentLocations.hasNext()) {
			Location where = adjacentLocations.next();
			Actor actor = field.getActorAt(where);
			// if an actor in an adjacent location is a rabbit
			// cast the actor as a rabbit and give it a 50% chance of
			// being infected with the virus.
			if (actor instanceof Rabbit) {
				Rabbit rabbit = (Rabbit) actor;
				if (rabbit.isAlive()) {
					if (rand.nextInt(100) < 50) {
						rabbit.setInfected();
						System.out
								.println("Rabbit infected by a different rabbit!");
					} else {
						System.out
								.println("Rabbit avoids infection from a different rabbit!");
					}

				}
			}
		}
	}
}
